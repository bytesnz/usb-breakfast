# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2022-07-11

Initial version!

[0.1.1]: https://gitlab.com/bytesnz/usb-breakfast/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/bytesnz/usb-breakfast/tree/v0.1.0
