# <!=package.json name> <!=package.json version>

<!=package.json description>

[![license](https://bytes.nz/b/<!=package.json name>/custom?color=yellow&name=license&value=AGPL-3.0)](<!=package.json repository.url>/blob/main/LICENSE)
[![developtment time](https://bytes.nz/b/<!=package.json name>/custom?color=yellowgreen&name=development+time&value=~14+hours)](<!=package.json repository.url>/blob/main/.tickings)
[![contributor covenant](https://bytes.nz/b/<!=package.json name>/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](<!=package.json repository.url>/blob/main/CODE_OF_CONDUCT.md)
[![support development](https://bytes.nz/b/<!=package.json name>/custom?color=brightgreen&name=support+development&value=$$)](https://liberapay.com/MeldCE)

![USB Breakfast PCB Top](output/usb-breakfast_top.png "USB Breakfast PCB Top")
![USB Breakfast PCB Bottom](output/usb-breakfast_bottom.png "USB Breakfast PCB Bottom")

## Development

Feel free to post errors or feature requests to the project
[issue tracker](<!=package.json bugs.url>) or
[email](mailto:<!=package.json bugs.email>) them to us.
**Please submit security concerns as a
[confidential issue](<!=package.json bugs.url>?issue[confidential]=true)**

<!=CHANGELOG.md>
