# usb-breakfast 1.0.0

USB to 8 USART port board

[![license](https://bytes.nz/b/usb-breakfast/custom?color=yellow&name=license&value=AGPL-3.0)](git+https://gitlab.com/bytesnz/usb-breakfast.git/blob/main/LICENSE)
[![developtment time](https://bytes.nz/b/usb-breakfast/custom?color=yellowgreen&name=development+time&value=~14+hours)](git+https://gitlab.com/bytesnz/usb-breakfast.git/blob/main/.tickings)
[![contributor covenant](https://bytes.nz/b/usb-breakfast/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](git+https://gitlab.com/bytesnz/usb-breakfast.git/blob/main/CODE_OF_CONDUCT.md)
[![support development](https://bytes.nz/b/usb-breakfast/custom?color=brightgreen&name=support+development&value=$$)](https://liberapay.com/MeldCE)

![USB Breakfast PCB Top](output/usb-breakfast_top.png "USB Breakfast PCB Top")
![USB Breakfast PCB Bottom](output/usb-breakfast_bottom.png "USB Breakfast PCB Bottom")

## Development

Feel free to post errors or feature requests to the project
[issue tracker](https://gitlab.com/bytesnz/usb-breakfast/issues) or
[email](mailto:) them to us.
**Please submit security concerns as a
[confidential issue](https://gitlab.com/bytesnz/usb-breakfast/issues?issue[confidential]=true)**

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2022-07-11

Initial version!

[0.1.1]: https://gitlab.com/bytesnz/usb-breakfast/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/bytesnz/usb-breakfast/tree/v0.1.0

